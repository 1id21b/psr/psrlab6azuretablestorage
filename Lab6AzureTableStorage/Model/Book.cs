﻿
using Microsoft.Azure.Cosmos.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6AzureTableStorage.Model
{
    class Book : TableEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string User_Id{ get; set; }

        public Book()
        {

        }

        public Book(string title, string description, Guid u_id)
        {
            PartitionKey = Guid.NewGuid().ToString();
            RowKey = Guid.NewGuid().ToString();
            Title = title;
            Description = description;
            User_Id = u_id.ToString();
            Timestamp = DateTime.Now;
        }

        public Book(string id, string rowKey, string title, string description, Guid u_id)
        {
            PartitionKey = id;
            RowKey = rowKey;
            Title = title;
            Description = description;
            User_Id = u_id.ToString();
            Timestamp = DateTime.Now;
            ETag = "*";
        }

        public override string ToString()
        {
            return "Ksiazka - Id: " + PartitionKey + " , Klucz wiersza: " + RowKey + " , Tytul: " + Title + ", Opis: " + Description + (User_Id == Guid.Empty.ToString()? ", Niewypozyczona" : ", Wypozyczona");
        }
    }
}
