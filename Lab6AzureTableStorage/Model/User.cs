﻿using Microsoft.Azure.Cosmos.Table;
using Microsoft.Azure.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Lab6AzureTableStorage.Model
{
    class User : TableEntity
    {
        public string Name { get; set; }
        public string Surname {get;set;}

        public User()
        {
        }

        public User(string name, string surname)
        {
            PartitionKey = Guid.NewGuid().ToString();
            RowKey = Guid.NewGuid().ToString();
            Name = name;
            Surname = surname;
            Timestamp = DateTime.Now;
        }

        public User(string id, string rowKey ,string name, string surname)
        {
            PartitionKey = id;
            RowKey = rowKey;
            Name = name;
            Surname = surname;
            Timestamp = DateTime.Now;
            ETag = "*";
        }

        public override string ToString()
        {
            return "Uzytkownik - Id: " + PartitionKey + " , Klucz wiersza: " + RowKey + " , Imie: " + Name + ", Nazwisko: " + Surname;
        }
    }
}
