﻿using Microsoft.Azure.Cosmos.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6AzureTableStorage
{
    static class Initialize
    {
        public static async Task<CloudTable> Init(string tableName)
        {
            string connectionString = "DefaultEndpointsProtocol=https;AccountName=marynat;AccountKey=VnUOpQrrvifV0I37m1CPHeTxnBWBw6LtOwFIzeM2d0cqK5W57X6vjbApXhFohIxdVHTCe0bSLAapuWBxPPbKog==;TableEndpoint=https://marynat.table.cosmos.azure.com:443/;";
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient(new TableClientConfiguration());

            CloudTable table = tableClient.GetTableReference(tableName);
            if (await table.CreateIfNotExistsAsync())
            {
                Console.WriteLine("Created Table named: {0}", tableName);
            }
            else
            {
                Console.WriteLine("Table {0} already exists", tableName);
            }
            return table;
        }
    }
}
