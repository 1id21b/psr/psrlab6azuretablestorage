﻿using Lab6AzureTableStorage.Model;
using Microsoft.Azure.Cosmos.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6AzureTableStorage.Repo
{
    class RepoForAll
    {

        public static void InsertOrMerge<T>(CloudTable table, T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            try
            {
                TableOperation insertOrMergeOperation = TableOperation.InsertOrMerge((ITableEntity)entity);
                TableResult result = table.Execute(insertOrMergeOperation);
                T insertedCustomer = (T)result.Result;
                if (result.RequestCharge.HasValue)
                {
                    Console.WriteLine("Request Charge of InsertOrMerge Operation: " + result.RequestCharge);
                }
            }
            catch (StorageException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                throw;
            }
        }

        public static void Replace<T>(CloudTable table, T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }

            try
            {
                TableOperation insertOrMergeOperation = TableOperation.Replace((ITableEntity)entity);
                TableResult result = table.Execute(insertOrMergeOperation);
                T insertedCustomer = (T)result.Result;
                if (result.RequestCharge.HasValue)
                {
                    Console.WriteLine("Request Charge of Replace Operation: " + result.RequestCharge);
                }
            }
            catch (StorageException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                throw;
            }
        }

        public static T GetEntity<T>(CloudTable table, string pk, string rk) where T : TableEntity, new()
        {
            try
            {
                TableOperation retrieveOperation = TableOperation.Retrieve<T>(pk, rk);
                TableResult result = table.Execute(retrieveOperation);
                T entity = (T)result.Result;
                if (entity != null)
                {
                    Console.WriteLine(entity);
                }

                if (result.RequestCharge.HasValue)
                {
                    Console.WriteLine("Request Charge of Retrieve Operation: " + result.RequestCharge);
                }

                return entity;
            }
            catch (StorageException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                throw;
            }
        }

        public static void GetAllEntities<T>(CloudTable table, int takeCount = 1000) where T : TableEntity, new()
        {
            try
            {
                var result = new List<T>();
                var query = new TableQuery<T>();
                query.TakeCount = takeCount;
                TableContinuationToken tableContinuationToken = null;
                do
                {
                    var queryResponse = table.ExecuteQuerySegmented(query, tableContinuationToken);
                    tableContinuationToken = queryResponse.ContinuationToken;
                    result.AddRange(queryResponse.Results);
                } while (tableContinuationToken != null);

                foreach (var b in result)
                {
                    Console.WriteLine(b);
                }
            }
            catch (StorageException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                throw;
            }
        }

        public static void DeleteEntity<T>(CloudTable table, T entity) where T : TableEntity, new()
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("deleteEntity");
                }

                TableOperation deleteOperation = TableOperation.Delete(entity);
                TableResult result = table.Execute(deleteOperation);

                if (result.RequestCharge.HasValue)
                {
                    Console.WriteLine("Request Charge of Delete Operation: " + result.RequestCharge);
                }

            }
            catch (StorageException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                throw;
            }
        }

        public static void ShowUserBooks(CloudTable table, string userId)
        {
            int takeCount = 1000;
            try
            {
                var result = new List<Book>();
                var query = new TableQuery<Book>().Where(TableQuery.GenerateFilterCondition("User_Id", QueryComparisons.Equal, userId));
                query.TakeCount = takeCount;
                TableContinuationToken tableContinuationToken = null;
                do
                {
                    var queryResponse = table.ExecuteQuerySegmented(query, tableContinuationToken);
                    tableContinuationToken = queryResponse.ContinuationToken;
                    result.AddRange(queryResponse.Results);
                } while (tableContinuationToken != null);

                foreach (var b in result)
                {
                    Console.WriteLine(b);
                }
            }
            catch (StorageException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                throw;
            }
        }

        public static void RentBook(CloudTable table, string id)
        {


            int takeCount = 1000;
            try
            {
                var result = new List<Book>();
                var query = new TableQuery<Book>().Where(TableQuery.GenerateFilterCondition("User_Id", QueryComparisons.Equal, Guid.Empty.ToString()));
                query.TakeCount = takeCount;
                TableContinuationToken tableContinuationToken = null;
                do
                {
                    var queryResponse = table.ExecuteQuerySegmented(query, tableContinuationToken);
                    tableContinuationToken = queryResponse.ContinuationToken;
                    result.AddRange(queryResponse.Results);
                } while (tableContinuationToken != null);

                foreach (var b in result)
                {
                    Console.WriteLine(b);
                }

                Console.WriteLine("Podaj id ksiazki do wypozyczenia");
                string s = Console.ReadLine();
                Console.WriteLine("Podaj klucz wiersza ksiazki do wypozyczenia");
                string r = Console.ReadLine();

                Book book = RepoForAll.GetEntity<Book>(table, s, r);
                book.User_Id = id;
                RepoForAll.Replace<Book>(table, book);
            }
            catch (StorageException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                throw;
            }
        }

        public static void ReturnBook(CloudTable table, string id, string rowKey)
        {
            try
            {
                Book book = RepoForAll.GetEntity<Book>(table, id, rowKey);
                book.User_Id = Guid.Empty.ToString();
                RepoForAll.Replace<Book>(table, book);
            }
            catch (StorageException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                throw;
            }
        }
    }
}