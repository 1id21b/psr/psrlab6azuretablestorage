﻿using Lab6AzureTableStorage.Model;
using Lab6AzureTableStorage.Repo;
using Microsoft.Azure.Cosmos.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6AzureTableStorage
{
    class Program
    {
        static async Task Main(string[] args)
        {


            CloudTable Books = await Initialize.Init("Books");
            CloudTable Users = await Task.Run(() => Initialize.Init("Users"));

            string menu = "";
            while (menu != "q")
            {
                MainMenu();
                menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        BookMenu(Books);
                        continue;
                    case "2":
                        UserMenu(Users, Books);
                        continue;
                    case "3":
                        RentMenu(Books);
                        continue;
                }
            }

        }

        static void MainMenu()
        {
            Console.WriteLine("Menu glowne:");
            Console.WriteLine("1. Menu Ksiazki");
            Console.WriteLine("2. Menu Uzytkownika");
            Console.WriteLine("3. Menu Wypozyczenia");
            Console.WriteLine("q. Wyjście");
        }
        static void BookMenu(CloudTable table)
        {
            string menu = "";
            while (menu != "q")
            {
                Console.WriteLine("Menu ksiazki:");
                Console.WriteLine("1. Dodaj");
                Console.WriteLine("2. Wyswietl wszystkie ksiązki");
                Console.WriteLine("3. Wyswietl pojedyńczy");
                Console.WriteLine("4. Edytuj");
                Console.WriteLine("5. Usun");
                Console.WriteLine("q. Wyjscie");
                menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        Console.WriteLine("Podaj Tytuł");
                        string Title = Console.ReadLine();
                        Console.WriteLine("Podaj Opis");
                        string Description = Console.ReadLine();
                        Book b = new Book(Title, Description, Guid.Empty);
                        RepoForAll.InsertOrMerge<Book>(table, b);
                        continue;
                    case "2":
                        RepoForAll.GetAllEntities<Book>(table);
                        continue;
                    case "3":
                        Console.WriteLine("Podaj id ksiazki");
                        string id = Console.ReadLine();
                        Console.WriteLine("Podaj klucz wiersza ksiazki");
                        string row = Console.ReadLine();
                        RepoForAll.GetEntity<Book>(table, id, row);
                        continue;
                    case "4":
                        Console.WriteLine("Podaj id ksiazki");
                        id = Console.ReadLine();
                        Console.WriteLine("Podaj klucz wiersza ksiazki");
                        row = Console.ReadLine();
                        Console.WriteLine("Podaj nowy tytul ksiazki");
                        string title = Console.ReadLine();
                        Console.WriteLine("Podaj nowy opis");
                        string description = Console.ReadLine();
                        Book bk = new Book(id, row, title, description, Guid.Empty);
                        RepoForAll.Replace<Book>(table, bk);
                        continue;
                    case "5":
                        Console.WriteLine("Podaj id ksiazki");
                        id = Console.ReadLine();
                        Console.WriteLine("Podaj klucz wiersza ksiazki");
                        row = Console.ReadLine();
                        bk = RepoForAll.GetEntity<Book>(table, id, row);
                        RepoForAll.DeleteEntity<Book>(table, bk);
                        continue;
                }
            }
        }
        static void UserMenu(CloudTable userTable, CloudTable bookTable)
        {
            string menu = "";
            while (menu != "q")
            {
                Console.WriteLine("Menu uzytkownika:");
                Console.WriteLine("1. Dodaj");
                Console.WriteLine("2. Wyswietl");
                Console.WriteLine("3. Wyswietl pojedyńczego");
                Console.WriteLine("4. Edytuj");
                Console.WriteLine("5. Usun");
                Console.WriteLine("6. Wyswietl ksiazki uzytkownika");
                Console.WriteLine("q. Wyjscie");
                menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        Console.WriteLine("Podaj imie uzytkownika");
                        string Name = Console.ReadLine();
                        Console.WriteLine("Podaj nazwisko uzytkownika");
                        string Surname = Console.ReadLine();
                        User u = new User(Name, Surname);
                        RepoForAll.InsertOrMerge<User>(userTable, u);
                        continue;
                    case "2":
                        RepoForAll.GetAllEntities<User>(userTable);
                        continue;
                    case "3":
                        Console.WriteLine("Podaj id uzytkownika");
                        string id = Console.ReadLine();
                        Console.WriteLine("Podaj klucz wiersza uzytkownika");
                        string rowkey = Console.ReadLine();
                        RepoForAll.GetEntity<User>(userTable, id, rowkey);
                        continue;
                    case "4":
                        Console.WriteLine("Podaj id uzytkownika");
                        id = Console.ReadLine();
                        Console.WriteLine("Podaj klucz wiersza uzytkownika");
                        rowkey = Console.ReadLine();
                        Console.WriteLine("Podaj nowe imie uzytkownika");
                        string name = Console.ReadLine();
                        Console.WriteLine("Podaj nowe nazwisko uzytkownika");
                        string surname = Console.ReadLine();
                        u = new User(id, rowkey, name, surname);
                        RepoForAll.Replace<User>(userTable, u);
                        continue;
                    case "5":
                        Console.WriteLine("Podaj id uzytkownika");
                        id = Console.ReadLine();
                        Console.WriteLine("Podaj klucz wiersza uzytkownika");
                        rowkey = Console.ReadLine();
                        u = RepoForAll.GetEntity<User>(userTable, id, rowkey);
                        RepoForAll.DeleteEntity<User>(userTable, u);
                        continue;
                    case "6":
                        Console.WriteLine("Podaj id uzytkownika");
                        id = Console.ReadLine();

                        //u = RepoForAll.GetEntity<User>(table, id, rowkey);
                        RepoForAll.ShowUserBooks(bookTable, id);
                        continue;
                }
            }
        }
        static void RentMenu(CloudTable table)
        {
            string menu = "";
            while (menu != "q")
            {
                Console.WriteLine("Menu Wypozyczenia:");
                Console.WriteLine("1. Wypozycz");
                Console.WriteLine("2. Oddaj");
                Console.WriteLine("q. Wyjscie");
                menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        Console.WriteLine("Podaj id uzytkownika");
                        string u = Console.ReadLine();
                        RepoForAll.RentBook(table, u);
                        continue;
                    case "2":
                        Console.WriteLine("Podaj id ksiazki");
                        u = Console.ReadLine();
                        Console.WriteLine("Podaj klucz wiersza ksiazki");
                        string rowkey = Console.ReadLine();
                        RepoForAll.ReturnBook(table, u, rowkey);
                        continue;
                }
            }
        }
    }
}